#!/usr/bin/env bash

function docker-compose-exec {
    docker-compose exec php "$@"
}

case $1 in
    "init")
        echo -e "\e[33m\nBuilding...\n\e[0m"

        docker-compose build
        ;;
    "up")
        echo -e "\e[33m\nUruchamianie aplikacji...\n\e[0m"

        docker-compose up -d
        ;;
    "down")
        echo -e "\e[33m\nZatrzymywanie uruchomionej aplikacji...\n\e[0m"

        docker-compose down
        ;;
    "restart")
        echo -e "\e[33m\nZatrzymywanie uruchomionej aplikacji...\n\e[0m"

        docker-compose down

        echo -e "\e[33m\nUruchamianie aplikacji...\n\e[0m"

        docker-compose up -d

        ;;
    "php")
       docker-compose-exec "$@"
        ;;
    "symfony")
       docker-compose-exec "$@"
        ;;
    "composer")
       docker-compose-exec "$@"
        ;;
    *)
        echo -e "\e[33m\nNiepoprawne polecenie.\n\e[0m"
esac
