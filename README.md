# Symfony 5 Docker

Docker for Symfony 5 with PHP 8, MySQL  8, phpMyAdmin

# Polecenia docker-compose

## Zbudowanie kontenera
docker-compose build

## Start kontenera.
docker-compose up -d
Bez opcji -d będą widoczne logi - przydatne np. w sytuacji żeby sprawdzić czy MySQL działa poprawnie.

## Zatrzymanie kontenera
docker-compose down

## Wykonywanie poleceń PHP
np.

docker-compose exec php symfony check:requirements   
docker-compose exec php composer install

## Można też dostać się do kontera PHP i w nim uruchamiać polecenia:
docker-compose exec php /bin/bash

## Tak samo do bazy:
docker-compose exec database /bin/bash
mysql -u root -p symfony #hasło secret

## Konfiguracja bazy w .env
DATABASE_URL="mysql://root:secret@database:3306/symfony?serverVersion=8.0"

###################################

# Skrypt docker.sh
Dla uproszczenia wywoływania poleceń można skorzystać i rozwijać skrypt ./docker.sh

## zbudowanie kontera i utworzenie katalogu "app"
./docker.sh init

## start kontenera
./docker.sh up

## zatrzymanie kontenera
./docker.sh down

## restart kontenera
./docker.sh restart

## polecenia symfony
./docker.sh symfony check:requirements 
./docker.sh symfony new .
etc...

## polecenia PHP
./docker.sh php -v

## polecenia composer
./docker.sh composer install
./docker.sh composer req doctrine twig


# Apka
url: http://localhost:8080/


# phpMyAdmin
http://localhost:8081

serwer: database
użytkownik: root
hasło: secret


